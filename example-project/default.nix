let pkgs = import ./nixpkgs.nix {};
in pkgs.stdenv.mkDerivation {
    name = "nix-example-project";

    buildInputs = [
        (pkgs.haskellPackages.ghcWithPackages (ps: with ps; [
            http-server
        ]))
    ];

    src = ./source-code;

    buildPhase = ''
        ghc -Wall -Werror Main.hs
    '';

    installPhase = ''
        install -D Main $out/bin/nix-example-project
    '';
}
