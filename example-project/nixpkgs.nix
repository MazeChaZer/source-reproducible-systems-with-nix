import (builtins.fetchTarball {
    name = "nixos-20.03-2257e6cf4d7fe7d0618ae4a040799a8604e186e5";
    url = "https://github.com/nixos/nixpkgs/archive/2257e6cf4d7fe7d0618ae4a040799a8604e186e5.tar.gz";
    sha256 = "06rdf5mccp15xv2ydr5y9ipbpkvs2yz9p9cv0iwik4hwm7dsld7a";
})
