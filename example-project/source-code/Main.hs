import Data.Function ((&))
import Codec.Binary.UTF8.String
import Network.URL
import Network.HTTP.Server
import Network.HTTP.Server.Logger

main :: IO ()
main = serverWith serverConfig $ \_ url request ->
    pure $ case url_path url of
        "" ->
            case rqMethod request of
                GET ->
                    sendText OK "Hello Nix!"

                _ ->
                    sendText MethodNotAllowed "HTTP method not allowed."

        _ ->
            sendText NotFound $ "Page not found: \"" <> url_path url <> "\""
    where
        serverConfig = defaultConfig
            { srvHost = "0.0.0.0"
            , srvLog = stdLogger
            }

sendText :: StatusCode -> String -> Response String
sendText statusCode content =
    (respond statusCode :: Response String) { rspBody = encodedContent }
        & insertHeader HdrContentLength (show (length encodedContent))
        & insertHeader HdrContentEncoding "UTF-8"
        & insertHeader HdrContentEncoding "text/plain"
    where encodedContent = encodeString content
