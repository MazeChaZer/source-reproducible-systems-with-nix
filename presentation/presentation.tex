\documentclass{beamer}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]
\usepackage[newfloat]{minted}
\usepackage{caption}
\usepackage{tikz}
\usetikzlibrary {arrows.meta,bending,positioning}
\tikzset{
    arrow/.style={-{Latex[length=2.5mm]}},
    dependency/.style={arrow,in=180,out=0},
    transitive dependency/.style={arrow,in=0,out=0},
    store item/.style={draw,minimum width=2.8cm,minimum height=0.6cm,text width=2.4cm},
    big store item/.style={draw,minimum width=8cm,minimum height=0.6cm,text width=7.6cm},
    ghost/.style={black!25}
}
\usepackage{svg}

\newenvironment{code}{\captionsetup{type=listing}}{}
\SetupFloatingEnvironment{listing}{name=Listing}
\resetcounteronoverlays{listing}

\usepackage[style=numeric-comp]{biblatex}
\bibliography{literatur.bib}

\AtBeginEnvironment{minted}{\dontdofcolorbox}
\def\dontdofcolorbox{\renewcommand\fcolorbox[4][]{##4}}


\title{Source-Reproducible Systems With Nix}
\author{Jonas Schürmann}
\date{2020-12-04}
\titlegraphic{\includesvg[scale=.1]{images/nixos}}

\begin{document}
    \frame{\titlepage}
    \begin{frame}
        \frametitle{What is Nix?}
        \begin{itemize}
            \item Generally: Nix is a build tool for system environments
                  (“\texttt{make}~for~systems”)
            \pause
            \item Nix is a functional package manager
            \pause
            \item Nix (the language) is a functional DSL to describe builds
                  and systems
            \pause
            \item NixOS is a Linux distribution
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{The Fundamental Build Block: Derivations}
        \begin{figure}
            \only<1>{\includegraphics[height=.8\textheight]{images/derivations-without-hashes-1.pdf}}
            \only<2>{\includegraphics[height=.8\textheight]{images/derivations-without-hashes-2.pdf}}
            \only<3>{\includegraphics[height=.8\textheight]{images/derivations-with-hashes.pdf}}
        \end{figure}
    \end{frame}

    \begin{frame}
        \frametitle{Building Derivations}
        \begin{figure}
            \only<1>{\includegraphics[height=.8\textheight]{images/building-derivations-1.pdf}}
            \only<2>{\includegraphics[height=.8\textheight]{images/building-derivations-2.pdf}}
            \only<3>{\includegraphics[height=.8\textheight]{images/building-derivations-3.pdf}}
            \only<4>{\includegraphics[height=.8\textheight]{images/building-derivations-4.pdf}}
            \only<5>{\includegraphics[height=.8\textheight]{images/building-derivations-5.pdf}}
            \only<6>{\includegraphics[height=.8\textheight]{images/building-derivations-6.pdf}}
            \only<7>{\includegraphics[height=.8\textheight]{images/building-derivations-7.pdf}}
        \end{figure}
    \end{frame}

    \begin{frame}
        \frametitle{Substitute Servers}
        \begin{figure}
            \includegraphics[height=.8\textheight]{images/substitutes.pdf}
        \end{figure}
    \end{frame}

    \begin{frame}
        \frametitle{The Nix Language}
        \begin{itemize}
            \item Variables
                  \mint{text}{let a = 1; in ...}
            \item String interpolation
                  \mint{text}{"a = ${a}"}
            \item Lists
                  \mint{text}{[ 1 2 3 ]}
            \item Records
                  \mint{text}{{ a = 1; b = 2; c = 3; }}
                  \mint{text}{{ foo.bar = 4; } == { foo = { bar = 4; }; }}
            \item Function definitions
                  \mint{text}{a: b: c: a + b + c}
                  \mint{text}{{ a, b, c }: a + b + c}
            \item Function applications
                  \mint{text}{f 1 2 3}
                  \mint{text}{f { a = 1; b = 2; c = 3; }}
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Local Development Environments}
        \centering
        \includegraphics[height=.8\textheight]{memes/patrick-dev-environment}
    \end{frame}

    \begin{frame}
        \frametitle{Local Development Environments}
        \begin{itemize}
            \item \texttt{nix-shell} creates temporary shell sessions where
                  the required dependencies are available
            \pause
            \item \texttt{nix-shell} let's you “dive” into derivations
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{System Deployment}
        \centering
        \includegraphics[height=.8\textheight]{memes/patrick-server}
    \end{frame}

    \begin{frame}
        \frametitle{System Deployment}
        \begin{itemize}
            \item Describe the whole system with Nix
            \item Everything is built with derivations
                  \begin{itemize}
                      \item Executables
                      \item Configuration files
                      \item Startup scripts
                      \item Bootloader
                      \item ...
                  \end{itemize}
        \end{itemize}
    \end{frame}

    \input{nixos-scenario}

    \begin{frame}
        \frametitle{Continous Integration}
        \centering
        \includegraphics[height=.8\textheight]{memes/patrick-ci-pipeline}
    \end{frame}

    \begin{frame}
        \frametitle{Continous Integration}
        \begin{itemize}
            \item “Just run \texttt{nix-build} on a system where Nix is
                  installed”
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Docker}
        \centering
        \includegraphics[height=.8\textheight]{memes/patrick-docker}
    \end{frame}

    \begin{frame}
        \frametitle{Docker}
        \begin{itemize}
            \item Nix can package derivations as Docker images
                  (without~using~Docker!)
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Conference artifacts}
        \centering
        \includegraphics[height=.8\textheight]{memes/patrick-conference}
    \end{frame}

    \begin{frame}
        \frametitle{Conference artifacts}
        \begin{itemize}
            \item TACAS'19: Artifacts must
                  \begin{itemize}
                      \item Run on a Ubuntu 18.04 VM
                      \item Include all dependencies
                      \item Work offline
                  \end{itemize}
                  % https://etaps.org/user-profile/list-articles/390-artifact-evaluation-tacas19
            \item Package derivation as a portable binary with
                  \texttt{nix-bundle}
            \pause
            \item Even better: Source-Reproducible Artifacts
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{GNU Guix}
        \begin{figure}
            \includesvg[scale=.4]{images/Guix}
        \end{figure}
        \begin{itemize}
            \item Fork of NixOS (2012)
            \item Replaced Nix + Shell with Guile (Scheme)
            \item Different goals
                  \begin{itemize}
                      \item User freedom
                      \item Bootstrapping the whole OS from source
                      \item Hackability
                      \item Well written documentation
                  \end{itemize}
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Trade-Offs}
        \begin{itemize}
            \item Higher initial investment
                  \begin{itemize}
                      \item Build tool and process has to be integrated with Nix
                      \item New paradigm, many new things to learn
                  \end{itemize}
            \pause
            \item Only runs on Linux, macOS and WSL; no native Windows
                  support (yet?)
                  % https://discourse.nixos.org/t/nix-on-windows/1113/13
            \pause
            \item Documentation is improving, but still lacking in some areas
        \end{itemize}
    \end{frame}

    \begin{frame}
        \frametitle{Conclusion}
        \begin{itemize}
            \item With Nix we can build
                  \begin{itemize}
                      \item Software packages
                      \item Development environments
                      \item System deployments
                      \item CI pipelines
                      \item Docker images
                      \item Conference artifacts
                      \item ...
                  \end{itemize}
            \pause
            \item Using an expressive, declarative DSL
            \pause
            \item Systems built with Nix are
                  \begin{itemize}
                      \item Immutable
                      \item Source-reproducible and automated
                      \item Cachable and substitutable
                  \end{itemize}
        \end{itemize}
    \end{frame}
\end{document}
