let pkgs = import ./nixpkgs.nix {};
    nix-example-project = import ../example-project;
in pkgs.dockerTools.buildImage {
    name = "nix-example-project";
    contents = [
        nix-example-project
        pkgs.iana-etc
    ];
    config = {
        Cmd = "/bin/nix-example-project";
        ExposedPorts = { "8000" = {}; };
    };
}
