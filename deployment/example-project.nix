{ config, pkgs, ... }:
let nix-example-project = import ../example-project;
    parameters = import ./parameters.nix;
in {
    networking.firewall.allowedTCPPorts = [ 80 443 ];

    systemd.services.nix-example-project = {
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
            DynamicUser = true;
            ExecStart = "${nix-example-project}/bin/nix-example-project";
        };
    };

    services.nginx = {
        enable = true;
        virtualHosts = {
            "nix-example-project.jonas-schuermann.name" = {
                addSSL = true;
                enableACME = true;
                locations."/" = {
                    proxyPass = "http://127.0.0.1:8000";
                };
            };
        };
    };

    security.acme = {
        email = parameters.acmeEmail;
        acceptTerms = true;
    };
}
