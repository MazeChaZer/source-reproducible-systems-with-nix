let parameters = import ./parameters.nix;
in {
    network = {
        pkgs = import ./nixpkgs.nix {};
    };

    nix-example-project = { config, pkgs, ... }: {
        imports = [
            ./hardware-configuration.nix
            ./system.nix
            ./example-project.nix
        ];

        deployment = {
            targetHost = parameters.server;
            targetUser = "root";
        };
    };
}
